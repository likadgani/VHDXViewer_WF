﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VHDXViewer_WF
{
    class Editor
    {
        public static string GetStr(string file)
        {
            List<string> result_list = new List<string>();
            int count = file.Length / 16;

            for (int i = 0; i < count; i++)
            {
                string tmp = file.Substring(i * 16, 16);
                result_list.Add(tmp);
            }
            string tm = file.Substring(count * 16, file.Length % 16);
            result_list.Add(tm);

            string Lines = "";
            string result_l = " ";
            for (int i = 0; i < result_list.Count; i++)
            {
                result_list[i].Replace('\r', '\0');
                foreach (char letter in result_list[i])
                {
                    result_l += " " + letter;
                }
                Lines += "\r\n" + result_l;
                result_l = " ";
            }
            return Lines;
        }
 
        public static string GetHexStr(string file)
        {
            char[] str_array = file.ToCharArray();
            
            int N = 16;
            int count = file.Length / N;
            List<string> result_list_hex = new List<string>();
            for (int i = 0; i < count; i++)
            {
                string tmp_hex = file.Substring(i * N, N);
                string _hex = "";
                foreach (char letter in tmp_hex)
                {
                    int value = Convert.ToInt32(letter);
                    _hex +=" "+String.Format("{0:X}", value);
                }
                result_list_hex.Add(_hex);
            }
            string tm = file.Substring(count * N, file.Length % N);
            string s_hex = "";
            foreach (char letter in tm)
            {
                int value = Convert.ToInt32(letter);
                s_hex += " " + String.Format("{0:X}", value);
            }
            result_list_hex.Add(s_hex);
            char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
            string Lines = "\t00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F";
            if (N > result_list_hex.Count)
                N = result_list_hex.Count;
            for (int i = 0; i < N; i++)
            {
                Lines += "\r\n0000000" + hex[i] + " " + result_list_hex[i];
            }
            return Lines;
        }
    }
}
