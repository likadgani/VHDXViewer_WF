﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
namespace VHDXViewer_WF
{
    public class FileReader
    {
        [DllImport("DLL.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto)]
        public static extern int OpenF(string p, out IntPtr hf);

        [DllImport("DLL.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        //public static extern void ReadF(IntPtr hf, int size, [param: MarshalAs(UnmanagedType.LPStr), Out()]out StringBuilder str);
        [return: MarshalAs(UnmanagedType.LPStr)]
        public static extern string ReadF(IntPtr hf, int size);

        [DllImport("DLL.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CloseF(IntPtr hf);
    }
}
