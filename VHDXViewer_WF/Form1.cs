﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//"My Change"
namespace VHDXViewer_WF
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void addFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog fileSelectDialog = new OpenFileDialog())
            {
                fileSelectDialog.Multiselect = false;
                fileSelectDialog.CheckFileExists = true;
                fileSelectDialog.FilterIndex = 0;
                fileSelectDialog.FileOk += fileSelectDialog_FileOk;
                fileSelectDialog.ShowDialog();
            }
        }

        void fileSelectDialog_FileOk(object sender, CancelEventArgs e)
        {
            listBox1.Items.Add((sender as OpenFileDialog).FileName);
        }


        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            string file = listBox1.SelectedItem.ToString();
            int result;
            IntPtr FileHandle;
            result = FileReader.OpenF(file, out FileHandle);
            string file_str = "";
            if (result != 0)
            {
                MessageBox.Show("Error : Cannot open the file!");
            }
            else
            { file_str = FileReader.ReadF(FileHandle, 1000); }
            result = FileReader.CloseF(FileHandle);

            textBox2.Text=Editor.GetStr(file_str);
            textBox1.Text = Editor.GetHexStr(file_str);

        }
    }

}